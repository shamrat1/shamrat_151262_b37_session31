<?php
trait Hello{
    public function sayHello(){
        echo 'hello[]';
    }
    public function smallTalk(){
        echo "a";
    }
    public function bigTalk(){
        echo "A";
    }
}
trait World{
    public function sayWorld(){
        echo "world[]";
    }

    public function smallTalk(){
        echo "b";
    }
    public function bigTalk(){
        echo "B";
    }
}
trait C{
    public function smallTalk(){
        echo "c";
    }
}
class MyHelloWorld{
    use Hello,World;
    public function sayExclamationMark(){
        echo "!";
    }
}
class Talker{
    use Hello,World,C{
        Hello::smallTalk insteadof World,C;
        World::bigTalk insteadof Hello;
    }
}

$obj = new Talker();
$obj->smallTalk();
$obj->bigTalk();

