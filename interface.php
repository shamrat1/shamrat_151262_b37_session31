<?php
interface CanFly{
    public function fly();
}
interface CanSwim{
    public function swim();

}
class Bird{
    public function info(){
     echo "i m a {$this->name}\n.";
     echo "i m a bird .\n";
    }
}

class Penguine extends Bird implements CanSwim{
    public $name = "penguine";
    public function swim()
    {
        echo "i can swim. ";
    }
}

class Dove extends Bird implements CanFly{
    public $name = "Dove";

    public function fly()
    {
        // TODO: Implement fly() method.
        echo "i can fly. ";
    }
}
class Duck extends Bird implements CanFly ,CanSwim{
    public $name = "Duck";

    public function fly()
    {
        echo "i can fly. ";
    }
    public function swim()
    {
        echo "i can swim. ";
    }
}
function describe($bird){
    if($bird instanceof Bird){
        $bird->info();
    }
    if($bird instanceof CanFly){
        $bird->fly();
    }
    if($bird instanceof CanSwim){
        $bird->swim();
    }
    else{ die("Come on ,its not a bird");}

}
describe(new Duck());

//$obj = new Duck();
//$obj->info();
//$obj->fly();